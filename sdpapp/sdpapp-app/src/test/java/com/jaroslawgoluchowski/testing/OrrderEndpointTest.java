package com.jaroslawgoluchowski.testing;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.jaroslawgoluchowski.bl.OrrderService;
import com.jaroslawgoluchowski.dl.Orrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class OrrderEndpointTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrrderService orrderService;

    @Test
    public void testListOrdersEndpoint() throws Exception {
        final String endpoint = "/order/list";

        mockMvc.perform(get(endpoint)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testCreateOrdersEndpoint() throws Exception {
        final String endpoint = "/order";
        String address = "adres";
        String phone = "3121";
        Date date = new Date(2014,10,10);

        when(orrderService.createOrder(address,phone,date)).thenReturn(new Orrder(address,phone,date));

        mockMvc.perform(put(endpoint).param("address", address).param("contactPhone", phone).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print()).andExpect(status().isBadRequest());
    }



}
