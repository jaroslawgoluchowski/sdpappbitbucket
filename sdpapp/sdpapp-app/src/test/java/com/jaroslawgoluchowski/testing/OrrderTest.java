package com.jaroslawgoluchowski.testing;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.jaroslawgoluchowski.dl.Orrder;
import org.junit.Test;

import java.util.Date;

public class OrrderTest {

    @Test
    public void testEguals() {
        Orrder orrder1 = new Orrder("Cos", "4123", new Date(2018, 10, 10));
        Orrder orrder2 = new Orrder("Cos", "4123", new Date(2018, 10, 10));
        assertTrue(orrder1.equals(orrder2));
    }

    @Test
    public void notEguals() {
        Orrder orrder1 = new Orrder("Cos", "4123", new Date(2018, 10, 10));
        Orrder orrder2 = new Orrder("Cos", "3214", new Date(2018, 10, 12));
        assertFalse(orrder1.equals(orrder2));
    }

}
