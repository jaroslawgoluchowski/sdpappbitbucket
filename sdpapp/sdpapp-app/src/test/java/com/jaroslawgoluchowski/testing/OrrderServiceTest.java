package com.jaroslawgoluchowski.testing;

import static org.assertj.core.api.Assertions.*;

import com.jaroslawgoluchowski.bl.OrrderService;
import com.jaroslawgoluchowski.dl.Orrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class OrrderServiceTest {

    @Mock
    private OrrderService orrderService;

    @Test
    public void testCreateOrder() {
        String address = "Adres";
        String phone = "14323";
        Date date = new Date(2016, 10, 10) ;
        Mockito.when(orrderService.createOrder(address,phone,date)).thenReturn(new Orrder(address,phone,date));
        assertThat(orrderService.createOrder(address,phone,date)).isEqualTo(new Orrder(address,phone,date));
    }

}
