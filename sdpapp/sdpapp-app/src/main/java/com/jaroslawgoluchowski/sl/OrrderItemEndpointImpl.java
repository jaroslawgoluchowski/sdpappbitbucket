package com.jaroslawgoluchowski.sl;

import com.jaroslawgoluchowski.bl.OrrderItemService;
import com.jaroslawgoluchowski.dl.OrrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/orderitem")
public class OrrderItemEndpointImpl {

    @Autowired
    private OrrderItemService orrderItemService;

    @RequestMapping(value = "/{orderId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createOrrderItem(@PathVariable("orderId") Long orderId, @RequestBody OrrderItem orrderItem) {
        orrderItemService.createOrrderItem(orderId, orrderItem.getMealName(), orrderItem.getPrice());
    }

    @RequestMapping(path="/list", method = RequestMethod.GET)
    public List<OrrderItem> listOrderItems() {
       return orrderItemService.listOrderItems();
    }

    @RequestMapping(path="/{id}", method = RequestMethod.GET)
    public OrrderItem getOrderItemById(@PathVariable("id") Long id) {
        return orrderItemService.getOrderItemById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteOrderItemById(@PathVariable("id") Long id){
        orrderItemService.deleteOrderItemById(id);
    }

}
