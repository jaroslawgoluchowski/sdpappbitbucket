package com.jaroslawgoluchowski.sl;

import com.jaroslawgoluchowski.bl.OrrderService;
import com.jaroslawgoluchowski.dl.Orrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;



@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/order")
public class OrrderEndpointImpl {

    @Autowired
    private OrrderService orrderService;

    @RequestMapping(path="/list", method = RequestMethod.GET)
    public List<Orrder> getAllOrders(){
        return orrderService.listOrders();
    }

    @RequestMapping(path="/{id}", method = RequestMethod.GET)
    public Orrder getOrderById(@PathVariable("id") Long id) {
        return orrderService.getOrderById(id);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addOrder(@RequestBody Orrder orrder){
        orrderService.createOrder(orrder.getAddress(), orrder.getContactPhone(), orrder.getOrderDate());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteOrderById(@PathVariable("id") Long id){
        orrderService.deleteOrderById(id);
    }

    @RequestMapping(value="/{date1}/{date2}", method = RequestMethod.GET)
    public List<Orrder> getOrderByDates(@PathVariable("date1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date d1,
                                  @PathVariable("date2") @DateTimeFormat(pattern = "yyyy-MM-dd") Date d2) {
        return orrderService.getOrrderByDates(d1,d2);
    }

}
