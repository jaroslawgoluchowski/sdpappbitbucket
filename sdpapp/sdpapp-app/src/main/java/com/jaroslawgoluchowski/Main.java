package com.jaroslawgoluchowski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
@EnableJpaRepositories
public class Main {

    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);

    }

    @RequestMapping("/")
    public String hello() {
        return "<HTML><BODY><i>This is <b>MY SPRING BOOT </b> web application. Hello !!!</i></BODY></HTML>";
    }


}



