package com.jaroslawgoluchowski.dl;

import org.springframework.data.repository.CrudRepository;

public interface OrrderItemRepo extends CrudRepository<OrrderItem, Long> {
    OrrderItem findByMealName(String mealName);
}
