package com.jaroslawgoluchowski.dl;

import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Orrder {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long orderNumber;
    private Date orderDate;
    private String address;
    private String contactPhone;
    @OneToMany
    private List<OrrderItem> orrderItems;

    public Orrder() {};

    public Orrder(Long orderNumber, Date orderDate, String address, String contactPhone, List<OrrderItem> orrderItems) {
        this.orderNumber = orderNumber;
        this.orderDate = orderDate;
        this.address = address;
        this.contactPhone = contactPhone;
        this.orrderItems = orrderItems;
    }

    public Orrder(String address, String contactPhone, Date orderDate) {
        this.address = address;
        this.contactPhone = contactPhone;
        this.orderDate = orderDate;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public List<OrrderItem> getOrrderItems() {
        return orrderItems;
    }

    public void setOrrderItems(List<OrrderItem> orrderItems) {
        this.orrderItems = orrderItems;
    }

    public void addItemToList(OrrderItem orrderItem){
        orrderItems.add(orrderItem);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(obj, this);
    }
}
