package com.jaroslawgoluchowski.dl;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;


public interface OrrderRepo extends CrudRepository<Orrder, Long> {
    @Query("SELECT order From Orrder order WHERE orderDate >= ?1 AND orderDate < ?2")
    List<Orrder> findByDates(Date orderDate1, Date orderDate2);
}
