package com.jaroslawgoluchowski.dl;

import javax.persistence.*;

@Entity
public class OrrderItem {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Orrder order;
    private String mealName;
    private int price;

    public OrrderItem() {};

    public OrrderItem(Long id, Orrder order, String mealName, int price) {
        this.id = id;
        this.order = order;
        this.mealName = mealName;
        this.price = price;
    }

    public Orrder getOrder() {
        return order;
    }

    public void setOrder(Orrder order) {
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
