package com.jaroslawgoluchowski.bl;

import com.jaroslawgoluchowski.dl.Orrder;
import com.jaroslawgoluchowski.dl.OrrderItem;
import com.jaroslawgoluchowski.dl.OrrderItemRepo;
import com.jaroslawgoluchowski.dl.OrrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrrderItemService {

    @Autowired
    private OrrderItemRepo orrderItemRepo;

    @Autowired
    private OrrderRepo orrderRepo;

    public OrrderItem createOrrderItem(Long orderId, String mealName, int price) {
        OrrderItem orrderItem = new OrrderItem();
        Orrder orrder = orrderRepo.findById(orderId).get();
        orrderItem.setMealName(mealName);
        orrderItem.setPrice(price);
        orrderItem.setOrder(orrder);
        orrderItem = orrderItemRepo.save(orrderItem);
        return orrderItem;
    }

    public List<OrrderItem> listOrderItems() {
        List<OrrderItem> orrderItems = new ArrayList<>();
        orrderItemRepo.findAll().forEach(orrder -> orrderItems.add(orrder));
        return orrderItems;
    }

    public void deleteOrderItemById(Long id){
        orrderItemRepo.deleteById(id);
    }

    public OrrderItem getOrderItemById(Long id) { return orrderItemRepo.findById(id).get(); }
}
