package com.jaroslawgoluchowski.bl;

import com.jaroslawgoluchowski.dl.Orrder;
import com.jaroslawgoluchowski.dl.OrrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrrderService {

    @Autowired
    private OrrderRepo orrderRepo;


    public Orrder createOrder(String address, String contactPhone, Date orderDate) {
        Orrder orrder = new Orrder();
        if(orderDate == null){
            orrder.setOrderDate(new Date());
        }else {
            orrder.setOrderDate(orderDate);
        }
        orrder.setAddress(address);
        orrder.setContactPhone(contactPhone);
        orrder = orrderRepo.save(orrder);
        return orrder;
    }

    public List<Orrder> listOrders() {
        List<Orrder> orrders = new ArrayList<>();
        orrderRepo.findAll().forEach(orrder -> orrders.add(orrder));
        return orrders;
    }

    public void deleteOrderById(Long id){
        orrderRepo.deleteById(id);
    }

    public Orrder getOrderById(Long id) { return orrderRepo.findById(id).get(); }

    public List<Orrder> getOrrderByDates( Date d1, Date d2) { return orrderRepo.findByDates(d1,d2); }
}
