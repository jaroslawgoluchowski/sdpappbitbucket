CREATE SEQUENCE public.my_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 99999
  START 1
  CACHE 1;
ALTER TABLE public.my_seq
  OWNER TO postgres;

CREATE TABLE public."ORDER"
(
  "ORDER_NUMBER" integer NOT NULL DEFAULT nextval('my_seq'::regclass),
  "ORDER_DATE" date,
  "ADDRESS" character varying(80),
  "CONTACT_PHONE" character varying(10),
  CONSTRAINT p_key PRIMARY KEY ("ORDER_NUMBER")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."ORDER"
  OWNER TO postgres;

CREATE TABLE public."ORDER_ITEM"
(
  "ID" integer NOT NULL,
  "ORDER_ID" integer,
  "MEAL_NAME" character varying(20),
  "PRICE" integer,
  CONSTRAINT pri_key PRIMARY KEY ("ID"),
  CONSTRAINT f_key FOREIGN KEY ("ORDER_ID")
      REFERENCES public."ORDER" ("ORDER_NUMBER") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."ORDER_ITEM"
  OWNER TO postgres;

INSERT INTO public."ORDER"("ORDER_DATE", "ADDRESS", "CONTACT_PHONE")
    VALUES ('2018-05-19', 'Koszalin Zwyciestwa 15', '05123412');

INSERT INTO public."ORDER"("ORDER_DATE", "ADDRESS", "CONTACT_PHONE")
    VALUES ('2018-05-15', 'Poznan Wolnosci 30', '5135123');

INSERT INTO public."ORDER"("ORDER_DATE", "ADDRESS", "CONTACT_PHONE")
    VALUES ('2018-04-01', 'Warszawa Mieszka I 12', '0341231');

INSERT INTO public."ORDER_ITEM"("ID", "ORDER_ID", "MEAL_NAME", "PRICE")
    VALUES ('1', '1', 'schabowy', '15');

INSERT INTO public."ORDER_ITEM"("ID", "ORDER_ID", "MEAL_NAME", "PRICE")
    VALUES ('2', '2', 'mielony', '8');

INSERT INTO public."ORDER_ITEM"("ID", "ORDER_ID", "MEAL_NAME", "PRICE")
    VALUES ('3', '3', 'frytki', '5');




